<?php
/**
 * Plugin Name: Testing Updates
 * Plugin URI: http://www.mywebsite.com/my-first-plugin
 * Description: The very first plugin that I have ever created.
 * Version: 2.0
 * Author: Your Name
 * Author URI: http://www.mywebsite.com
 */
require __DIR__ . '/vendor/autoload.php';


$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
	'https://gitlab.com/ryanemitchell/testing-updates',
	__FILE__,
	'testing-updates'
);

//Optional: If you're using a private repository, specify the access token like this:
$myUpdateChecker->setAuthentication('your-token-here');

//Optional: Set the branch that contains the stable release.
$myUpdateChecker->setBranch('stable-branch-name');



 add_action( 'the_content', 'my_thank_you_text' );

function my_thank_you_text ( $content ) {
    return $content .= '<p>Thank you for readings!</p>';
}
